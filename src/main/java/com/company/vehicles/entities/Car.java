package com.company.vehicles.entities;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Car {
    private String CarBrand;
    private String CarClass;
    private int wight;
    public Car(String CR, String CC, int w) {
        CR = CarBrand;
        CC = CarClass;
        w = wight;

    }

    Car car = new Car("Toyota","X4",1100);
    Driver driver = new Driver(5);
    Engine motor = new Engine("239 к.с. (178 кВт) при 5200 об/хв", "Camigo Plant");

    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String carBrand) {
        CarBrand = carBrand;
    }

    public String getCarClass() {
        return CarClass;
    }

    public void setCarClass(String carClass) {
        CarClass = carClass;
    }

    public int getWight() {
        return wight;
    }

    public void setWight(int wight) {
        this.wight = wight;
    }

    public void start(){
        System.out.println("Поїхали");
    }
    public void stop(){
        System.out.println("Стоп");
    }
    public void turnRight(){
        System.out.println("Поворот направо");
    }
    public void turnLeft(){
        System.out.println("Поворот наліво");
    }
    public String toString(){
        return "Мотор : " + motor + "Водій : " + driver + "Авто : " + car;
    }
}
